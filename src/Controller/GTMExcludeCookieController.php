<?php
namespace Drupal\gtm_exclude_cookie\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Defines GTMExcludeCookieController class.
 */
class GTMExcludeCookieController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function set() {

    $headers = ['Cache-Control' => 'no-cache'];
    $response = new RedirectResponse('gtm-exclude-status', 302, $headers);

    $gtm_exclude_cookie = new Cookie(
      'STYXKEY-gtm_exclude_cookie',
      strtotime('now + 10 years'),
      strtotime('now + 10 years'),
      '/', // Path.
      null, // Domain.
      false, // Xmit secure https.
      false // HttpOnly
    );
 
    $response->headers->setCookie($gtm_exclude_cookie);
    $response->headers->clearCookie('STYXKEY_gtm_exclude_cookie', '/', null);

    return $response;
  }

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function status() {
    // should this be rendered by js so we aren't fighting with Drupal's caching?
    if (isset($_COOKIE['STYXKEY-gtm_exclude_cookie'])) {
        // @TODO: Do we need to check to see if this is a valid timestamp?
        $gtm_exclude_cookie_exp = date('m/d/Y H:i:s', $_COOKIE['STYXKEY-gtm_exclude_cookie']);
        return [
            '#type' => 'markup',
            '#markup' => $this->t('Your GTM Exclude cookie is set and will expire on ' . $gtm_exclude_cookie_exp),
          ];
    }
    else {
        
        return [
            '#type' => 'markup',
            '#markup' => $this->t('Your GTM Exclude cookie is not set. <a href="/gtm-exclude">Use this link to set it</a>.'),
          ];

    }
  }
}

